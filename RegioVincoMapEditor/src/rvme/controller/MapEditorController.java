package rvme.controller;

/**
 * @author Calvin Li 
 * 
 */

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import rvme.gui.Workspace;
import properties_manager.PropertiesManager;
import rvme.data.DataManager;
import rvme.data.Subregion;
import saf.AppTemplate;
import rvme.gui.EditSubregionDialog;

/**
 * @author Calvin Li 109535588 
 * CSE214
 */

public class MapEditorController {

/**
 * This class responds to interactions with map viewing controls.
 *
 * @author McKillaGorilla
 * @author Calvin Li
 * @version 1.0
 */

    AppTemplate app;

    PropertiesManager props = PropertiesManager.getPropertiesManager();
    

    public MapEditorController(AppTemplate initApp) {
        app = initApp;
    }
    
//    public void zoomHandler(MouseButton button, double x, double y) {
//        //center
//        Workspace workspace = (Workspace) app.getWorkspaceComponent();
//        workspace.reloadWorkspace();   
//        switch (button) {
//            case PRIMARY: 
//                //then zoom in 
//                workspace.sc.setPivotX(x);
//                workspace.sc.setPivotY(y);
//                workspace.sc.setX(2);
//                workspace.sc.setY(2);
//                workspace.getWork().getTransforms().add(workspace.sc);
//                workspace.getWork().setScaleX(workspace.getWork().getScaleX() * 2.0);
//                workspace.getWork().setScaleY(workspace.getWork().getScaleY() * 2.0);
//                workspace.getWork().setLayoutX(workspace.getWork().getLayoutX() + app.getGUI().getPrimaryScene().getWidth()/2-x);
//                workspace.getWork().setLayoutY(workspace.getWork().getLayoutY() + app.getGUI().getPrimaryScene().getHeight()/2-y);
//
//                break;
//            case SECONDARY:
//                //zoom out
//                workspace.getWork().setScaleX(workspace.getWork().getScaleX() / 2.0);
//                workspace.getWork().setScaleY(workspace.getWork().getScaleY() / 2.0);
//                workspace.getWork().setLayoutX(workspace.getWork().getLayoutX() + app.getGUI().getPrimaryScene().getWidth()/2-x);
//                workspace.getWork().setLayoutY(workspace.getWork().getLayoutY() + app.getGUI().getPrimaryScene().getHeight()/2-y);
//        }
//        workspace.reloadWorkspace();
//    }

    
    public void keyPressHandler(KeyCode e) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getWork().requestFocus();
        switch (e) {
            case UP: workspace.getPolyGroup().setTranslateY(workspace.getPolyGroup().getTranslateY()+20);
                break; 
            case DOWN: workspace.getPolyGroup().setTranslateY(workspace.getPolyGroup().getTranslateY()-20);
                break;
            case LEFT: workspace.getPolyGroup().setTranslateX(workspace.getPolyGroup().getTranslateX()-20);
                break;
            case RIGHT:workspace.getPolyGroup().setTranslateX(workspace.getPolyGroup().getTranslateX()+20);
                break;
            default:
                break;
        }
    }
    
    public void processAddEditItem() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace();
        EditSubregionDialog dialog = new EditSubregionDialog();
        dialog.init(new Stage(), workspace.getTable().getSelectionModel().getSelectedItem(), app);
        Subregion regionToEdit = workspace.getTable().getSelectionModel().getSelectedItem();
        
        dialog.setFields(dialog.getSubregionName(),dialog.getSubregionCapital(),dialog.getLeaderName());
        dataManager.getRegions().add(workspace.getTable().getSelectionModel().getFocusedIndex(), regionToEdit);
        workspace.reloadWorkspace();
    }
}