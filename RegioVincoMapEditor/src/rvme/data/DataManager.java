package rvme.data;

/**
 * @author Calvin Li 
 * 
 */

import java.util.ArrayList;
import java.util.Collections;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import saf.components.AppDataComponent;
import rvme.MapEditorApp;
import rvme.controller.MapEditorController;
import rvme.gui.Workspace;
import saf.AppTemplate;
import rvme.gui.EditSubregionDialog;

/**
 * @author Calvin Li
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    
    MapEditorController meController;
    // Region-based variables (NOT SUB-REGION)
    private String regionName;
    private String backgroundColor;
    private String borderColor;
    private double borderThickness;
    
    public double width;
    public double height;
    
    // Lists of Sub-regions, images
    private Polygon selectedRegion;
    public ObservableList<Subregion> subregions;
    ObservableList<Polygon> polygons;
    ArrayList<Integer> grayscaleColors;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    public DataManager(AppTemplate initApp) {
        app = initApp;
        meController = new MapEditorController(app);
        subregions = FXCollections.observableArrayList();
        polygons = FXCollections.observableArrayList();
        grayscaleColors = new ArrayList<>();
        for (int i = 1; i < 254; i++) {
            grayscaleColors.add(i);
        }
        width = 802;
        height = 536;
//        images = FXCollections.observableArrayList();
    }
    
    @Override
    public void reset() {
//        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        subregions.clear();
        polygons.clear();
//        images.clear();
    }    
        
    public double getWidth() {
        return width;
    }
    
    public double getHeight() {
//        double totalHeight = app.getGUI().getWindow().getScene().getHeight();
//        double toolbarHeight = app.getGUI().getAppPane().getTop().getLayoutBounds().getHeight();
//        return totalHeight-toolbarHeight;
        return height;
    }
    
    public double getFullHeight() {
        double totalHeight = app.getGUI().getWindow().getScene().getHeight();
        double toolbarHeight = app.getGUI().getAppPane().getTop().getLayoutBounds().getHeight();
        return totalHeight-toolbarHeight;
    }
    
    public ObservableList<Subregion> getRegions() {
        return subregions;
    }
    
    public void addSubregion(Subregion subregion) {
        subregions.add(subregion);
    }
    
//    public ObservableList<String> getImages() {
//        return images;
//    }
//    
//    public void addImage(String image) {
//        images.add(image);
//    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public double getBorderThickness() {
        return borderThickness;
    }

    public void setBorderThickness(double borderThickness) {
        this.borderThickness = borderThickness;
    }
    
    public void addPolygon(Polygon polygon) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        subregions.add(new Subregion("","","",""));
        workspace.subregions.add(new Subregion("","","",""));
        polygon.setOnMouseClicked(e -> {
            selectedRegion = polygon;
            if (e.getClickCount() == 2) {
                polygon.requestFocus();
                meController.processAddEditItem();
            }
        });
        polygons.add(polygon);        
        polygon.setFill(Color.OLIVEDRAB);
        polygon.setStroke(Color.BLACK);
        polygon.setStrokeWidth(0.05);
        polygon.strokeProperty().bind(workspace.getBorderColorPicker().valueProperty());
        polygon.strokeWidthProperty().bind(workspace.getBorderThicknessSlider().valueProperty());
        VBox leftBox = (VBox)workspace.splitPane.getItems().get(0);
        Pane pane = (Pane)leftBox.getChildren().get(1);
        Pane innerpane = (Pane)pane.getChildren().get(1);
        Group polyGroup = (Group)innerpane.getChildren().get(0);
        polyGroup.setScaleX(15);
        polyGroup.setScaleY(15);
        polyGroup.getChildren().add(polygon);
    }
    
    public void randomizePolyColors(){
        Collections.shuffle(grayscaleColors);
        for (int i = 0; i < polygons.size(); i++) {
            polygons.get(i).setFill(Color.rgb(grayscaleColors.get(i),grayscaleColors.get(i),grayscaleColors.get(i)));
        }
    }

    public Polygon getSelectedRegion() {
        return selectedRegion;
    }

    public void setSelectedRegion(Polygon selectedRegion) {
        this.selectedRegion = selectedRegion;
    }
    
    public double getZoomValue() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        return workspace.getZoomSlider().getValue();
    }
    
    public double getWorkspaceX(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        return workspace.getPolyGroup().getTranslateX();
    }    
    public double getWorkspaceY(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        return workspace.getPolyGroup().getTranslateY();
    }
}