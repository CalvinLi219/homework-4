package rvme.data;


/**
 * @author Calvin Li 
 * 
 */

public class Subregion {
    private String subName;
    private String subCapital;
    private String leaderName;
    private String greyscaleColor;
    
    public Subregion(){
        subName = "";
        subCapital = "";
        leaderName = "";
        greyscaleColor = "";
    }
    
    public Subregion(String subName, String subCapital, String leaderName, String greyscaleColor) {
        this.subName = subName;
        this.subCapital = subCapital;
        this.leaderName = leaderName;
        this.greyscaleColor = greyscaleColor;
    }

    public Subregion(String name, String capital, String leader) {
        subName = name;
        subCapital = capital;
        leaderName = leader;
    }
    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getSubCapital() {
        return subCapital;
    }

    public void setSubCapital(String subCapital) {
        this.subCapital = subCapital;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public String getGreyscaleColor() {
        return greyscaleColor;
    }

    public void setGreyscaleColor(String greyscaleColor) {
        this.greyscaleColor = greyscaleColor;
    }
    
    public void reset() {
//        setSubName(defaultsubname);
//        etc
    }
}
