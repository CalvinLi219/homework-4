package rvme.file;

/**
 * @author Calvin Li 
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import rvme.MapEditorApp;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.ui.NewMapDialogSingleton;

/**
 * @author Calvin Li
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    AppTemplate app;
    static int ptCounter;
    double width;
    double height;
    Double[] ArrPolygonPts;

    NewMapDialogSingleton newMapSingleton = NewMapDialogSingleton.getSingleton();
    
    static final String JSON_SUBNAME = "sub_name";
    static final String JSON_SUBCAPITAL = "sub_capital";
    static final String JSON_LEADER_NAME = "leader_name";
    static final String JSON_GREYSCALE_COLOR = "greyscale_color";
    
    static final String JSON_REGION_NAME = "region_name";
    static final String JSON_BG_COLOR = "bg_color";
    static final String JSON_BORDER_COLOR = "border_color";
    static final String JSON_BORDER_THICKNESS = "border_thickness";
    static final String JSON_ZOOM_VALUE = "zoom_value";    
    static final String JSON_SUBREGIONS = "subregions";
    static final String JSON_IMAGES = "images";
    static final String JSON_IMAGE = "image";
    static final String JSON_SCROLL_X = "scroll_x_value";
    static final String JSON_SCROLL_Y = "scroll_y_value";
    

    public void loadInfo(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
//	dataManager.reset();
//        Workspace workspace = (Workspace)app.getWorkspaceComponent();

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// LOAD VARIABLES
        String region = json.getString(JSON_REGION_NAME);
        String backgroundColor = json.getString(JSON_BG_COLOR);
        String borderColor = json.getString(JSON_BORDER_COLOR);
        double borderThickness = getDataAsDouble(json, JSON_BORDER_THICKNESS);
        
        dataManager.setRegionName(region);
        dataManager.setBackgroundColor(backgroundColor);
	dataManager.setBorderColor(borderColor);
        dataManager.setBorderThickness(borderThickness);
        
	// AND NOW LOAD ALL THE ITEMS
	JsonArray jsonItemArray = json.getJsonArray(JSON_SUBREGIONS);
	for (int i = 0; i < jsonItemArray.size(); i++) {
	    JsonObject jsonItem = jsonItemArray.getJsonObject(i);
	    Subregion subregion = loadSubregion(jsonItem);
	    dataManager.addSubregion(subregion);
//            TableView table = (TableView)workspace.splitPane.getItems().get(1);
//            table.getItems().add(subregion);
	}
    }
    
    public Subregion loadSubregion(JsonObject jsonItem) {
	// GET THE DATA
        String subName = jsonItem.getString(JSON_SUBNAME);
        String subCapital = jsonItem.getString(JSON_SUBCAPITAL);
        String leaderName = jsonItem.getString(JSON_LEADER_NAME);
        String greyscaleColor = jsonItem.getString(JSON_GREYSCALE_COLOR);
        
	// THEN USE THE DATA TO BUILD AN ITEM
        Subregion subregion = new Subregion(subName,subCapital,leaderName,greyscaleColor);
        
	// ALL DONE, RETURN IT
	return subregion;
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    public JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	DataManager dataManager = (DataManager)data;
//	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	// FIRST THE VARIABLES
	String region = dataManager.getRegionName();
        String backgroundColor = dataManager.getBackgroundColor();
        String borderColor = dataManager.getBorderColor();
        double borderThickness = dataManager.getBorderThickness();
        double zoomValue = dataManager.getZoomValue();
        double scrollValueX = dataManager.getWorkspaceX();
        double scrollValueY = dataManager.getWorkspaceX();
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	ObservableList<Subregion> subs = dataManager.getRegions();
	for (Subregion subregion : subs) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBNAME, subregion.getSubName())
		    .add(JSON_SUBCAPITAL, subregion.getSubCapital())
		    .add(JSON_LEADER_NAME, subregion.getLeaderName())
//		    .add(JSON_LEADER_IMAGE, subregion.getLeaderImage())  
		    .add(JSON_GREYSCALE_COLOR, subregion.getGreyscaleColor())
                    .build();
	    arrayBuilder.add(itemJson);
	}
	JsonArray subArray = arrayBuilder.build();
        
//	// NOW BUILD THE JSON ARRAY FOR THE IMAGES
//	JsonArrayBuilder arrBuilder = Json.createArrayBuilder();
//	ObservableList<String> imgs = dataManager.getImages();
//	for (String image : imgs) {	    
//	    JsonObject itemJson = Json.createObjectBuilder()
//		    .add(JSON_IMAGE, image)
//                    .build();
//	    arrBuilder.add(itemJson);
//	}
//	JsonArray imgArray = arrBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_REGION_NAME, region)
                .add(JSON_BG_COLOR, backgroundColor)
		.add(JSON_BORDER_COLOR, borderColor)
		.add(JSON_BORDER_THICKNESS, borderThickness)
		.add(JSON_ZOOM_VALUE, zoomValue)
                .add(JSON_SUBREGIONS, subArray)
		.add(JSON_SCROLL_X, scrollValueX)
                .add(JSON_SCROLL_Y, scrollValueY)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
//        this.saveData(data, "./HW5SampleData/work/Andorra.rvm");
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Polygon loadPolygon(JsonObject jsonObject) {
        // GET THE DATA
        double x = getDataAsDouble(jsonObject, "X");
        double y = getDataAsDouble(jsonObject, "Y");

        // THEN USE THE DATA TO BUILD AN ITEM
        Polygon poly = new Polygon(x,y);

        // ALL DONE, RETURN IT
        return poly;
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
//        dataManager.reset();
        width = dataManager.getWidth();
        height = dataManager.getHeight();
        Polygon polygon;
                        
        JsonObject json = loadJSONFile(filePath);
        JsonArray subregionsArray = json.getJsonArray("SUBREGIONS");
        for (int i = 0; i < subregionsArray.size(); i++){
            JsonObject region = subregionsArray.getJsonObject(i);
            JsonArray subregionPolygons = region.getJsonArray("SUBREGION_POLYGONS");
            
            for (int j = 0; j < subregionPolygons.size(); j++) {
                JsonArray pts = subregionPolygons.getJsonArray(j);
                ptCounter = 0;
                polygon = new Polygon();
                ArrPolygonPts = new Double[pts.size() * 2];
                
                for (int k = 0; k < pts.size(); k++) {
                    JsonObject pt = pts.getJsonObject(k);
                    Double x = getDataAsDouble(pt, "X");
                    Double y = getDataAsDouble(pt, "Y");
                    ArrPolygonPts[ptCounter] = (((x+180)/360)*width);
                    ptCounter += 1;
                    ArrPolygonPts[ptCounter] = (((y+90)/180)*height);
                    ptCounter += 1; 
                }
                polygon.getPoints().addAll(ArrPolygonPts);
                dataManager.addPolygon(polygon);
            }
        }
    }
    
}
