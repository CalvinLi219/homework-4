/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polygon;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rvme.data.DataManager;
import rvme.data.Subregion;
import saf.AppTemplate;

/**
 *
 * @author Calvin Li
 */
public class EditSubregionDialog extends Stage {
        
    // GUI CONTROLS FOR OUR DIALOG
    TextField subNameTF;
    TextField subCapitalTF;
    TextField ldrNameTF;    
    Button okButton;
    boolean doWork = false;
    Subregion region;
    AppTemplate app;
    
    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public EditSubregionDialog() {}
    
	
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     * @param region
     * @param app
     */
    public void init(Stage primaryStage, Subregion region, AppTemplate app) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        DataManager d = (DataManager)app.getDataComponent();
        Workspace w = (Workspace)app.getWorkspaceComponent();
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.region = region;
        this.app = app;
        primaryStage.setTitle("Edit Subregion Dialog");
        //edit subregion details
        
        VBox topPane = new VBox();
        Label subName = new Label("Subregion name");
        Label subCapital = new Label("Subregion capital");
        Label ldrName = new Label("Leader name");
        subNameTF = new TextField();
        subCapitalTF = new TextField();
        ldrNameTF = new TextField();
        
        setupHandlers();
        
        //flag img, leader image
        Label flagImgName = new Label("(flag img) Picture not available");
        ImageView flagImg = new ImageView(new Image("file:./images/Missingfile.png"));
//        ImageView temp = new ImageView(new Image("file://"+
//                w.newMapSingleton.getSelectedDirectory().toString()+"/"+
//                w.newMapSingleton.getSelectedName()+"/"+w.newMapSingleton.getSelectedName()+" Flag.png"));
//        if (temp != null)
//            flagImg = temp;
        
        Label leaderImgName = new Label("(leader img) Picture not available");
        ImageView leaderImg = new ImageView(new Image("file:./images/Missingfile.png"));
//        ImageView temp2 = new ImageView(new Image("file://"+
//                w.newMapSingleton.getSelectedDirectory().toString()+"/"+
//                region.getLeaderName()+"/"+region.getLeaderName()+".png"));
//        if (temp2 != null)
//            flagImg = temp2;
                
        
        BorderPane imgPane = new BorderPane();
        HBox flags = new HBox();
        flags.getChildren().addAll(flagImgName,flagImg);
        HBox leaders = new HBox();
        leaders.getChildren().addAll(leaderImgName,leaderImg);
        imgPane.setTop(flags);
        imgPane.setBottom(leaders);
        
        topPane.getChildren().addAll(subName,subNameTF,subCapital,subCapitalTF,ldrName,ldrNameTF);
        topPane.setSpacing(5);
        
        //next subregion
        //previous subregion
        //close subregion dialog
        Button nextSubregion = new Button("Next");
        Button prevSubregion = new Button("Previous");
        nextSubregion.setOnAction(e->{
            int index = d.getRegions().indexOf(region);
            if (index < d.getRegions().size()-1) {
                this.close();
                EditSubregionDialog newDialog = new EditSubregionDialog();
                Subregion nextRegion = d.getRegions().get(index + 1);
                newDialog.init(primaryStage, nextRegion, app);
                newDialog.setFields(nextRegion.getSubName(), nextRegion.getSubCapital(), nextRegion.getLeaderName());
            }
            close();
        });       
        
        prevSubregion.setOnAction(e->{
            int index = d.getRegions().indexOf(region);
            if (index > 0) {
                this.close();
                EditSubregionDialog newDialog = new EditSubregionDialog();
                Subregion prevRegion = d.getRegions().get(index - 1);
                newDialog.init(primaryStage, prevRegion, app);
                newDialog.setFields(prevRegion.getSubName(), prevRegion.getSubCapital(), prevRegion.getLeaderName());
            }
            close();
        });
        
        HBox prevnextPane = new HBox();
        prevnextPane.getChildren().addAll(nextSubregion,prevSubregion);
        prevnextPane.setSpacing(10);
        
        HBox okPane = new HBox();
        okButton = new Button("Ok");
        
        okButton.setOnAction(e->{
            this.close();
        });
        
        okPane.getChildren().addAll(prevnextPane, okButton);
        okPane.setSpacing(30);
        
        VBox bottomPane = new VBox();
        bottomPane.getChildren().addAll(prevnextPane,okPane);
           
        VBox pane = new VBox();
        pane.getChildren().addAll(topPane,imgPane,bottomPane);
        pane.setSpacing(10);
        pane.setStyle("-fx-background-color: #7de5f4; -fx-text-fill: white;");
        pane.setPadding(new Insets(20,20,20,20));
        
        // AND PUT IT IN THE WINDOW
        Scene newScene = new Scene(pane);
        this.setScene(newScene);
        this.showAndWait();
    }

    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     */
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
    
    public String getSubregionName() {
        return subNameTF.getText();
    }
    public String getSubregionCapital() {
        return subCapitalTF.getText();
    }
    public String getLeaderName() {
        return ldrNameTF.getText();
    }
    
    public void setFields(String subregionName, String subregionCapital, String leaderName) {
        subNameTF.setText(subregionName);
        subCapitalTF.setText(subregionCapital);
        ldrNameTF.setText(leaderName);
    }
    
    public Button getOkButton() {
        return okButton;
    }
    
    public TextField getSubNameTextField() {
        return subNameTF;
    }   
    
    public TextField getSubCapitalTextField() {
        return subCapitalTF;
    }   
    
    public TextField getLeaderNameTF() {
        return ldrNameTF;
    }
    
    public void setupHandlers() {
        subNameTF.textProperty().addListener(e->{
            region.setSubName(subNameTF.getText());
            app.getWorkspaceComponent().reloadWorkspace();
        });
        subCapitalTF.textProperty().addListener(e->{
            region.setSubCapital(subCapitalTF.getText());
            app.getWorkspaceComponent().reloadWorkspace();
        }); 
        ldrNameTF.textProperty().addListener(e->{
          region.setLeaderName(ldrNameTF.getText());
            app.getWorkspaceComponent().reloadWorkspace();
        });
    }
}
