package rvme.gui;
/**
 * @author Calvin Li 
 * 
 */


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import saf.components.AppWorkspaceComponent;
import rvme.MapEditorApp;
import rvme.audio.AudioManager;
import rvme.controller.MapEditorController;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.file.FileManager;
import saf.AppTemplate;
import saf.ui.AppGUI;
import saf.ui.ChangeMapDimsDialog;
import saf.ui.ChangeMapNameDialog;
import saf.ui.NewMapDialogSingleton;

public class Workspace extends AppWorkspaceComponent {
    AppTemplate app;
    AppGUI gui;
    FileManager fm = new FileManager();
    DataManager dm;
    
    public SplitPane splitPane = new SplitPane();
    Group polyGroup;

    MapEditorController meController; 
        
    NewMapDialogSingleton newMapSingleton = NewMapDialogSingleton.getSingleton();
    Slider zoomSlider;
    Slider brdrThkSlider;
    ColorPicker brdrColPicker;
    AudioManager audio;
    Pane imagePane;
    ArrayList<ImageView> imageList = new ArrayList<>();
    int currentImageIndex = 0;
    TableView<Subregion> table;
    public ObservableList<Subregion> subregions;

    public Workspace(MapEditorApp initApp) {
        app = initApp;
        gui = app.getGUI();
        dm = (DataManager)app.getDataComponent();
        audio = new AudioManager();
        workspace = new Pane();
        subregions = FXCollections.observableArrayList();
        this.setupToolbar(gui);
        this.setupSplitpane(gui);
        setupHandlers();
    }

    @Override
    public void reloadWorkspace() {
        DataManager dataManager = (DataManager)app.getDataComponent();
        polyGroup.applyCss();
        table.refresh();
//        if(dataManager.getSelectedRegion()!= null) {
//            dataManager.getSelectedRegion().setStrokeType(StrokeType.OUTSIDE);
//        }
    }
    
    @Override
    public void initStyle() {
        this.getWork().setStyle("-fx-background-color: #5094D4; -fx-text-fill: white;");
    }
    
    public Pane getWork() {
        return this.workspace;
    }

    private void setupHandlers() {
        meController = new MapEditorController(app);
        //Handle behavior of "Ok" Button in New Map Dialog
        //load in data points
        newMapSingleton.getOkButton().setOnAction(e->{
            try {
                clearLeftPane();
                fm.loadData(dm, newMapSingleton.getJsonFile().toString());
                //create new directory, then new data file in directory
                String path = newMapSingleton.getSelectedDirectory().toString();
                path += "/";
                path += newMapSingleton.getSelectedName();
                new File(path).mkdirs();
                newMapSingleton.close();
            } catch (Exception ex) {
                newMapSingleton.close();
            }
        });
       
        app.getGUI().getPrimaryScene().setOnKeyPressed(e->{
            e.consume();
            meController.keyPressHandler(e.getCode());
        });
        
    }
    public void setupToolbar(AppGUI gui) {
        FlowPane toolbar = (FlowPane)gui.getAppPane().getTop();
//        meController = new MapEditorController(app);
        
        //sliders
        zoomSlider = new Slider(0,8,0.5);
        Label zoomLabel = new Label("Zoom");
        VBox zoomPane = new VBox();
        zoomPane.getChildren().addAll(zoomSlider,zoomLabel);
        zoomPane.setAlignment(Pos.CENTER);
        
        brdrThkSlider = new Slider(0,0.5,0.05);
        Label brdrThkLabel = new Label("Border Thickness");
        VBox brdrThkPane = new VBox();
        brdrThkPane.getChildren().addAll(brdrThkSlider,brdrThkLabel);
        brdrThkPane.setAlignment(Pos.CENTER);
        
        //color pickers
        ColorPicker bgColorPicker = new ColorPicker();
        bgColorPicker.setOnAction(e->{
            VBox leftPane = (VBox)splitPane.getItems().get(0);
            StackPane mapPane = (StackPane)leftPane.getChildren().get(1);
            Color tempColor = bgColorPicker.getValue();
            mapPane.setStyle("-fx-background-color: #" + tempColor.toString().substring(2, 8));
        });
        Label bgLabel = new Label("Background Color");
        VBox bgPane = new VBox();
        bgPane.getChildren().addAll(bgColorPicker, bgLabel);
        bgPane.setAlignment(Pos.CENTER);
        
        brdrColPicker = new ColorPicker();
        Label brdrColLabel = new Label("Border Color");
        VBox brdrColPane = new VBox();
        brdrColPane.getChildren().addAll(brdrColPicker, brdrColLabel);
        brdrColPane.setAlignment(Pos.CENTER);
        
        //buttons
        HBox btnPane = new HBox();
        btnPane.setSpacing(5);
        
        Button RenameMap = new Button();
        RenameMap.setGraphic(new ImageView(new Image("file:./images/RenameMap.png")));
        RenameMap.setTooltip(new Tooltip("Rename Map"));
        RenameMap.setOnAction(e->{
            ChangeMapNameDialog.getSingleton().show("New Dialog", "Change Map Name: ");
            String oldname = newMapSingleton.getSelectedName();
            String newname = ChangeMapNameDialog.getSingleton().getSelection();
            File file = new File(newMapSingleton.getSelectedDirectory() + "/" + oldname);
            File file2 = new File(newMapSingleton.getSelectedDirectory() + "/" + newname);
            file.renameTo(file2);
        });
        
        Button AddImg = new Button();
        AddImg.setGraphic(new ImageView(new Image("file:./images/Add.png")));
        AddImg.setTooltip(new Tooltip("Add Image"));
        AddImg.setOnAction(e->{
            FileChooser fc = new FileChooser();
            fc.setTitle("Add Image");
            fc.getExtensionFilters().addAll(
            new ExtensionFilter("Image (.png)", "*.png"));
            File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
            ImageView newImage = new ImageView();
            imageList.add(newImage);
            newImage.setOnMouseClicked(b->{
                currentImageIndex = imageList.indexOf(newImage);
            });
            newImage.setOnMouseDragged(a->{
                newImage.setX(a.getX());
                newImage.setY(a.getY());
            });
            if (selectedFile != null) {
                newImage.setImage(new Image(selectedFile.toURI().toString()));
                imagePane.getChildren().add(newImage);
            }
        });
        
        Button RmvImg = new Button();
        RmvImg.setGraphic(new ImageView(new Image("file:./images/Remove.png")));
        RmvImg.setTooltip(new Tooltip("Remove Image"));
        RmvImg.setOnAction(e->{
            imageList.remove(currentImageIndex);
            imagePane.getChildren().remove(currentImageIndex);
        });

        Button AudioBtn = new Button();
        AudioBtn.setGraphic(new ImageView(new Image("file:./images/AudioBtn.png")));
        AudioBtn.setTooltip(new Tooltip("Play Audio"));
        AudioBtn.setOnAction(e->{
            try {
                String song = "hi";
                if (audio.isPlaying(song)) {
                    audio.stop(song);
                } else {
                    //directory / name of region / nameofregion.
                    audio.loadAudio(song, newMapSingleton.getSelectedDirectory().toString() +
                            "/"+ newMapSingleton.getSelectedName() +"/"+
                            newMapSingleton.getSelectedName()+" National Anthem.mid");
                    audio.play(song, true);
                } 
            } catch(Exception excp) {
            }
        });
        
        Button RandomColors = new Button();
        RandomColors.setGraphic(new ImageView(new Image("file:./images/RandomColors.png")));
        RandomColors.setTooltip(new Tooltip("Randomize Map Colors"));
        RandomColors.setOnAction(e->{
            dm.randomizePolyColors();
            reloadWorkspace();
        });
        
        Button ChgMapDims = new Button();
        ChgMapDims.setGraphic(new ImageView(new Image("file:./images/ChgMapDims.png")));        //add new image
        ChgMapDims.setTooltip(new Tooltip("Change Map Dimensions"));
        ChgMapDims.setOnAction(e->{
            ChangeMapDimsDialog.getSingleton().show("Change Map Dimensions", "Enter a width", "Enter a height");
            //change datamanger dimensions
            dm.height = Integer.parseInt(ChangeMapDimsDialog.getSingleton().getUserHeight());
            dm.width = Integer.parseInt(ChangeMapDimsDialog.getSingleton().getUserWidth());
            //change workspace dimensions
            VBox leftpane = (VBox)splitPane.getItems().get(0);
            leftpane.setMaxHeight(dm.height);
            leftpane.setMaxWidth(dm.width);
            reloadWorkspace();
        });

        btnPane.getChildren().addAll(RenameMap,AddImg,RmvImg,AudioBtn,RandomColors,ChgMapDims);
        
        Button exportButton = new Button();
        exportButton.setGraphic(new ImageView(new Image("file:./images/Export.png")));
        exportButton.setTooltip(new Tooltip("Export to file"));
        exportButton.setOnAction(e->{
            //TODO
        });

        toolbar.getChildren().addAll(exportButton, zoomPane, brdrThkPane, bgPane, brdrColPane, btnPane);
        toolbar.setHgap(5);
    }
    public void setupSplitpane(AppGUI gui) {
        //Splitpane; map and table        
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        table = new TableView();
        table.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                meController.processAddEditItem();
            }
        });
        
        TableColumn nameCol = new TableColumn("Name"); 
        TableColumn capitalCol = new TableColumn("Capital");
        TableColumn ldrCol = new TableColumn("Leader");
        
        nameCol.setCellValueFactory(new PropertyValueFactory("subName"));        
        capitalCol.setCellValueFactory(new PropertyValueFactory("subCapital"));        
        ldrCol.setCellValueFactory(new PropertyValueFactory("leaderName"));
   
        table.getColumns().add(nameCol);
        table.getColumns().add(capitalCol);
        table.getColumns().add(ldrCol);
        table.setItems(subregions);
        
        VBox toplabelBox = new VBox();
        HBox sidelabelBox = new HBox();
        Pane pane = new Pane();
        pane.setTranslateY(100);
        pane.setScaleY(-1);
        polyGroup = new Group();
        pane.getChildren().add(polyGroup);
        StackPane mapPane = new StackPane();
        mapPane.scaleXProperty().bind(zoomSlider.valueProperty());
        mapPane.scaleYProperty().bind(zoomSlider.valueProperty());
        mapPane.getChildren().addAll(sidelabelBox,pane);
        imagePane = new Pane();
        VBox leftPane = new VBox();
        leftPane.getChildren().addAll(toplabelBox,mapPane,imagePane);
                
        splitPane.getItems().add(leftPane);
        splitPane.getItems().add(table);
        splitPane.setMinWidth(app.getGUI().getWindow().getWidth());
        splitPane.setMinHeight(dataManager.getFullHeight());
        workspace.getChildren().add(splitPane);
    }
    
    public Group getPolyGroup() {
        return polyGroup;
    }
    
    public void clearLeftPane() {
        polyGroup.getChildren().clear();
    }
    
    public Pane getLeftPane() {
        VBox leftPane = (VBox)splitPane.getItems().get(0);
        StackPane mapPane = (StackPane)leftPane.getChildren().get(1);
        Pane pane = (Pane)mapPane.getChildren().get(1);
        return pane;
    }
    
    public Slider getBorderThicknessSlider(){
        return brdrThkSlider;
    }
    
    public ColorPicker getBorderColorPicker(){
        return brdrColPicker;
    }
    
    public TableView<Subregion> getTable() {
        return table;
    }
    
    public Slider getZoomSlider() {
        return zoomSlider;
    }
}