package test_bed;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Calvin Li 
 * 
 */

public class EditSubregionDialog extends Application {
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        VBox pane = new VBox();
        primaryStage.setTitle("Edit Subregion Dialog");
        //edit subregion details
        
        VBox topPane = new VBox();
        Label subName = new Label("Subregion name");
        Label subCapital = new Label("Subregion capital");
        Label ldrName = new Label("Leader name");
        TextField subNameTF = new TextField();
        TextField subCapitalTF = new TextField();
        TextField ldrNameTF = new TextField();
        
        //flag img, leader image
        Label flagImgName = new Label("(flag img) Picture not available");
        Label leaderImgName = new Label("(leader img) Picture not available");
        Button flagImgButton = new Button();
        Button leaderImgButton = new Button();
        flagImgButton.setGraphic(new ImageView(new Image("file:./images/Open.png")));
        leaderImgButton.setGraphic(new ImageView(new Image("file:./images/Open.png")));
        BorderPane imgPane = new BorderPane();
        HBox flags = new HBox();
        flags.getChildren().addAll(flagImgName,flagImgButton);
        HBox leaders = new HBox();
        leaders.getChildren().addAll(leaderImgName,leaderImgButton);
        imgPane.setTop(flags);
        imgPane.setBottom(leaders);
        
        topPane.getChildren().addAll(subName,subNameTF,subCapital,subCapitalTF,ldrName,ldrNameTF);
        topPane.setSpacing(5);
        
        //next subregion
        //previous subregion
        //close subregion dialog
        Button nextSubregion = new Button("Next");
        Button prevSubregion = new Button("Previous");
        
        HBox prevnextPane = new HBox();
        prevnextPane.getChildren().addAll(nextSubregion,prevSubregion);
        prevnextPane.setSpacing(140);
        
        HBox okPane = new HBox();
        Button closeButton = new Button("Close");
        Button okButton = new Button("Ok");
        okPane.getChildren().addAll(prevnextPane, closeButton, okButton);
        okPane.setSpacing(168);
        
        VBox bottomPane = new VBox();
        bottomPane.getChildren().addAll(prevnextPane,okPane);
        
        pane.getChildren().addAll(topPane,imgPane,bottomPane);
        pane.setSpacing(10);
        pane.setStyle("-fx-background-color: #7de5f4; -fx-text-fill: white;");
        pane.setPadding(new Insets(20,20,20,20));
        primaryStage.setScene(new Scene(pane, 300, 380));
        primaryStage.show();
    }
}