package test_bed;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Calvin Li 
 * 
 */

public class NewMapDialog extends Application {
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        VBox pane = new VBox();
        primaryStage.setTitle("New Map Dialog");
        
        //topsection
        VBox topPane = new VBox();
        topPane.setSpacing(5);
        Label regionName = new Label("Region name: ");
        Label parentDir = new Label("Parent region directory: ");
        TextField regionNameTF = new TextField();
        TextField parentDirTF = new TextField();
        
        //midsection
        Label fileSelectLabel = new Label("Select a file(coords)");
        Button fileSelectBtn = new Button();
        fileSelectBtn.setGraphic(new ImageView(new Image("file:./images/Open.png")));
        
        BorderPane midPane = new BorderPane();
        HBox files = new HBox();
        files.getChildren().addAll(fileSelectLabel,fileSelectBtn);
        midPane.setTop(files);
        
        topPane.getChildren().addAll(regionName,regionNameTF,parentDir,parentDirTF);
        
        //next subregion
        //previous subregion
        //close subregion dialog
        Button nextSubregion = new Button("Next");
        Button prevSubregion = new Button("Previous");
        
        HBox prevnextPane = new HBox();
        prevnextPane.getChildren().addAll(nextSubregion,prevSubregion);
        prevnextPane.setSpacing(140);
        HBox okPane = new HBox();
        Button closeButton = new Button("Close");
        Button okButton = new Button("Ok");
        okPane.getChildren().addAll(prevnextPane, closeButton, okButton);
        okPane.setSpacing(168);
        
        VBox bottomPane = new VBox();
        bottomPane.getChildren().addAll(prevnextPane,okPane);
        
        pane.setSpacing(20);
        pane.getChildren().add(topPane);
        pane.getChildren().add(midPane);
        pane.getChildren().add(bottomPane);
        pane.setStyle("-fx-background-color: #7de5f4; -fx-text-fill: white;");
        pane.setPadding(new Insets(20,20,20,20));
        primaryStage.setScene(new Scene(pane, 300, 300));
        primaryStage.show();
    }
}