package test_bed;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import rvme.MapEditorApp;
import rvme.data.DataManager;
import rvme.file.FileManager;

/**
 * @author Calvin Li 
 * 
 */

public class TestLoad  {
    
    static MapEditorApp app;
    
    public static void main(String[] args) throws IOException {
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        System.out.println("----------");
        System.out.println("ANDORRA");
        System.out.println("----------");
        fileManager.loadInfo(dataManager, "./HW5SampleData/work/Andorra.json");
        printData(dataManager);
        System.out.println("----------");
        System.out.println("SAN MARINO");
        System.out.println("----------");
        fileManager.loadInfo(dataManager, "./HW5SampleData/work/San Marino.json");
        printData(dataManager);
        System.out.println("----------");
        System.out.println("SLOVAKIA");
        System.out.println("----------");
        fileManager.loadInfo(dataManager, "./HW5SampleData/work/Slovakia.json");
        printData(dataManager);
    }
    
    public static void printData(DataManager dm) {
        System.out.println("Background color: " + dm.getBackgroundColor());
        System.out.println("Border color: " + dm.getBorderColor());
        System.out.println("Border thickness: " + dm.getBorderThickness());
        System.out.println("Region name: " + dm.getRegionName());
        for(int i = 0; i < dm.getRegions().size(); i++) {
            System.out.println("Subregion Name: " + dm.getRegions().get(i).getSubName() + "  |  "
                    + "Subregion Capital: " + dm.getRegions().get(i).getSubCapital()  + "  |  "
                    + "Leader Name: " + dm.getRegions().get(i).getLeaderName() + "  |  "
                    + "Greyscale Color: " + dm.getRegions().get(i).getGreyscaleColor());
        }
    }

}
