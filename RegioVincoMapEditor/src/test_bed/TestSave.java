package test_bed;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import rvme.MapEditorApp;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.file.FileManager;

/**
 * @author Calvin Li 
 * 
 */

public class TestSave {
    
    static MapEditorApp app;
    
    static final String tempname = "n/a";
    
    public static void main(String[] args) {

        createAndorra();
        createSanMarino();
        createSlovakia();
    }
    
    public static void createAndorra() {
        //hard code creation of all data
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("Andorra");
//        dataManager.addImage("./HW5SampleData/work/andorracrest.json");
//        dataManager.addImage("./HW5SampleData/work/andorraflag.json");
        
        //add the 7 subregions (parishes)
        Subregion canillo = new Subregion("Canillo", "Canillo", "Enric Casadevall Medrano", "#BABABA");
        dataManager.addSubregion(canillo);
        Subregion ordino = new Subregion("Ordino", "Ordino","Ventura Espot i Beixanet", "#BCBCBC");
        dataManager.addSubregion(ordino);
        Subregion lamassana = new Subregion("La Massana", "La Massana","Josep Ma Camp Areny", "#B3B3B3");
        dataManager.addSubregion(lamassana);
        Subregion encamp = new Subregion("Encamp", "Encamp","Miquel Alis Font", "#B7B7B7");
        dataManager.addSubregion(encamp);
        Subregion escalades = new Subregion("Escalades Engordany", "Escalades Engordany","Montserrat Capdevila Pallares", "#B5B5B5");
        dataManager.addSubregion(escalades);
        Subregion lavella = new Subregion("Andorra la Vella", "Andorra la Vella","Maria Rosa Ferrer Obiols", "#B0B0B0");
        dataManager.addSubregion(lavella);
        Subregion santjulia = new Subregion("Sant Julia de Loria", "Sant Julia de Loria", "Josep Pintat Forne","#AEAEAE");
        dataManager.addSubregion(santjulia);
        
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/Andorra.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void createSanMarino() {
        //hard code creation of all data
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("San Marino");
        
        //add the 8 subregions (municipalities)
        Subregion acquaviva = new Subregion("Acquaviva", "Acquaviva", "Lucia Tamagnini", "#DADADA");
        dataManager.addSubregion(acquaviva);
        Subregion borgo = new Subregion("Borgo Maggiore", "Borgo Maggiore","Sergio Nanni", "#BCBCBC");
        dataManager.addSubregion(borgo);
        Subregion chiesanuova = new Subregion("Chiesanuova", "Chiesanuova","Franco Santi", "#9F9F9F");
        dataManager.addSubregion(chiesanuova);
        Subregion domagnano = new Subregion("Domagnano", "Domagnano","Gabriel Guidi", "#848484");
        dataManager.addSubregion(domagnano);
        Subregion faetano = new Subregion("Faetano", "Faetano","Pier Mario Bedetti", "#6A6A6A");
        dataManager.addSubregion(faetano);
        Subregion fiorentino = new Subregion("Fiorentino", "Fiorentino","Gerri Fabbri", "#515151");
        dataManager.addSubregion(fiorentino);
        Subregion montegiardino = new Subregion("Montegiardino", "Montegiardino", "Marta Fabbri","#3A3A3A");
        dataManager.addSubregion(montegiardino);
        Subregion serravalle = new Subregion("Serravalle", "Serravalle", "Leandro Maiani","#131313");
        dataManager.addSubregion(serravalle);
        Subregion sanmarino = new Subregion("City of San Marino", "City of San Marino", "Maria Teresa Beccari","#262626");
        dataManager.addSubregion(sanmarino);
        
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/San Marino.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void createSlovakia() {
        //hard code creation of all data
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("Slovakia");
//        dataManager.addImage("./HW5SampleData/work/slovakiacrest.json");
//        dataManager.addImage("./HW5SampleData/work/slovakiaflag.json");
        
        //add the 8 subregions (krajes)
        Subregion Bratislava = new Subregion("Bratislava", "Bratislava", tempname, "#F9F9F9");
        dataManager.addSubregion(Bratislava);
        Subregion Trnava = new Subregion("Trnava", "Trnava",tempname, "#F7F7F7");
        dataManager.addSubregion(Trnava);
        Subregion Trencin = new Subregion("Trencin", "Trencin",tempname, "#F6F6F6");
        dataManager.addSubregion(Trencin);
        Subregion Nitra = new Subregion("Nitra", "Nitra",tempname, "#F5F5F5");
        dataManager.addSubregion(Nitra);
        Subregion Zilina = new Subregion("Zilina", "Zilina",tempname, "#F4F4F4");
        dataManager.addSubregion(Zilina);
        Subregion Banska = new Subregion("Banska Bystrica", "Banska Bystrica",tempname, "#F2F2F2");
        dataManager.addSubregion(Banska);
        Subregion Presov = new Subregion("Presov", "Presov", tempname,"#F1F1F1");
        dataManager.addSubregion(Presov);
        Subregion Kosice = new Subregion("Kosice", "Kosice", tempname,"#F0F0F0");
        dataManager.addSubregion(Kosice);
        
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/Slovakia.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
