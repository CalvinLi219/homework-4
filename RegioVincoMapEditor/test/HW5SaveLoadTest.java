/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rvme.MapEditorApp;
import rvme.data.DataManager;
import rvme.data.Subregion;
import rvme.file.FileManager;

/**
 *
 * @author Calvin
 */
public class HW5SaveLoadTest {
    
    MapEditorApp app;

    @Test
    public void testAndorra() throws IOException {
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        
        //hard code creation of all data
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("Andorra");
        
        //add the 7 subregions (parishes)
        Subregion canillo = new Subregion("Canillo", "Canillo", "Enric Casadevall Medrano", "#BABABA");
        dataManager.addSubregion(canillo);
        Subregion ordino = new Subregion("Ordino", "Ordino","Ventura Espot i Beixanet", "#BCBCBC");
        dataManager.addSubregion(ordino);
        Subregion lamassana = new Subregion("La Massana", "La Massana","Josep Ma Camp Areny", "#B3B3B3");
        dataManager.addSubregion(lamassana);
        Subregion encamp = new Subregion("Encamp", "Encamp","Miquel Alis Font", "#B7B7B7");
        dataManager.addSubregion(encamp);
        Subregion escalades = new Subregion("Escalades Engordany", "Escalades Engordany","Montserrat Capdevila Pallares", "#B5B5B5");
        dataManager.addSubregion(escalades);
        Subregion lavella = new Subregion("Andorra la Vella", "Andorra la Vella","Maria Rosa Ferrer Obiols", "#B0B0B0");
        dataManager.addSubregion(lavella);
        Subregion santjulia = new Subregion("Sant Julia de Loria", "Sant Julia de Loria", "Josep Pintat Forne","#AEAEAE");
        dataManager.addSubregion(santjulia);
        
        //save data
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/Andorra.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
                        
        //load in data
        fileManager.loadData(dataManager, "./HW5SampleData/work/Andorra.json");

        //TEST
        assertEquals("#D15905",dataManager.getBackgroundColor());
        assertEquals("#000000",dataManager.getBorderColor());
        assertEquals("Andorra",dataManager.getRegionName());
        assertEquals("#BCBCBC",dataManager.getRegions().get(1).getGreyscaleColor());
        assertEquals("Enric Casadevall Medrano", dataManager.getRegions().get(0).getLeaderName());
    }
    
    @Test
    public void testSanMarino() throws IOException {
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        
        //hard code creation of all data
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("San Marino");
        
        //add the 8 subregions (municipalities)
        Subregion acquaviva = new Subregion("Acquaviva", "Acquaviva", "Lucia Tamagnini", "#DADADA");
        dataManager.addSubregion(acquaviva);
        Subregion borgo = new Subregion("Borgo Maggiore", "Borgo Maggiore","Sergio Nanni", "#BCBCBC");
        dataManager.addSubregion(borgo);
        Subregion chiesanuova = new Subregion("Chiesanuova", "Chiesanuova","Franco Santi", "#9F9F9F");
        dataManager.addSubregion(chiesanuova);
        Subregion domagnano = new Subregion("Domagnano", "Domagnano","Gabriel Guidi", "#848484");
        dataManager.addSubregion(domagnano);
        Subregion faetano = new Subregion("Faetano", "Faetano","Pier Mario Bedetti", "#6A6A6A");
        dataManager.addSubregion(faetano);
        Subregion fiorentino = new Subregion("Fiorentino", "Fiorentino","Gerri Fabbri", "#515151");
        dataManager.addSubregion(fiorentino);
        Subregion montegiardino = new Subregion("Montegiardino", "Montegiardino", "Marta Fabbri","#3A3A3A");
        dataManager.addSubregion(montegiardino);
        Subregion serravalle = new Subregion("Serravalle", "Serravalle", "Leandro Maiani","#131313");
        dataManager.addSubregion(serravalle);
        Subregion sanmarino = new Subregion("City of San Marino", "City of San Marino", "Maria Teresa Beccari","#262626");
        dataManager.addSubregion(sanmarino);
        
        //save data
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/San Marino.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
                        
        //load in data
        fileManager.loadData(dataManager, "./HW5SampleData/work/San Marino.json");

        //TEST
        assertEquals("#D15905",dataManager.getBackgroundColor());
        assertEquals("#000000",dataManager.getBorderColor());
        assertEquals("San Marino",dataManager.getRegionName());
        assertEquals("Faetano",dataManager.getRegions().get(4).getSubCapital());
        assertEquals("Lucia Tamagnini", dataManager.getRegions().get(0).getLeaderName());
    }
    
    @Test
    public void testSlovakia() throws IOException {
        DataManager dataManager = new DataManager(app);
        FileManager fileManager = new FileManager();
        
        //hard code creation of all data
        dataManager.setBackgroundColor("#D15905");
        dataManager.setBorderColor("#000000");
        dataManager.setBorderThickness(1.0);
        dataManager.setRegionName("Slovakia");
        
        //add the 8 subregions (krajes)
        Subregion Bratislava = new Subregion("Bratislava", "Bratislava", "n/a", "#F9F9F9");
        dataManager.addSubregion(Bratislava);
        Subregion Trnava = new Subregion("Trnava", "Trnava","n/a", "#F7F7F7");
        dataManager.addSubregion(Trnava);
        Subregion Trencin = new Subregion("Trencin", "Trencin","n/a", "#F6F6F6");
        dataManager.addSubregion(Trencin);
        Subregion Nitra = new Subregion("Nitra", "Nitra","n/a", "#F5F5F5");
        dataManager.addSubregion(Nitra);
        Subregion Zilina = new Subregion("Zilina", "Zilina","n/a", "#F4F4F4");
        dataManager.addSubregion(Zilina);
        Subregion Banska = new Subregion("Banska Bystrica", "Banska Bystrica","n/a", "#F2F2F2");
        dataManager.addSubregion(Banska);
        Subregion Presov = new Subregion("Presov", "Presov", "n/a","#F1F1F1");
        dataManager.addSubregion(Presov);
        Subregion Kosice = new Subregion("Kosice", "Kosice", "n/a","#F0F0F0");
        dataManager.addSubregion(Kosice);
        
        //save data
        try {
            fileManager.saveData(dataManager, "./HW5SampleData/work/Slovakia.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
                        
        //load in data
        fileManager.loadData(dataManager, "./HW5SampleData/work/Slovakia.json");

        //TEST
        assertEquals("#D15905",dataManager.getBackgroundColor());
        assertEquals("#000000",dataManager.getBorderColor());
        assertEquals("Slovakia",dataManager.getRegionName());
        assertEquals("Trencin",dataManager.getRegions().get(2).getSubName());
        assertEquals("#F9F9F9", dataManager.getRegions().get(0).getGreyscaleColor());
    }
    
}
