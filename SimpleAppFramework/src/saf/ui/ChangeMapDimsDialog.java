/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saf.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Calvin Li
 */
public class ChangeMapDimsDialog extends Stage {
    static ChangeMapDimsDialog singleton;
    
    // GUI CONTROLS FOR OUR DIALOG
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Label messageLabel2;
    TextField widthInput;
    TextField heightInput;
    Button doneButton;

    
    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    private ChangeMapDimsDialog() {}
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static ChangeMapDimsDialog getSingleton() {
	if (singleton == null)
	    singleton = new ChangeMapDimsDialog();
	return singleton;
    }
	
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();        
        messageLabel2 = new Label();        

        // YES, NO, AND CANCEL BUTTONS
        doneButton = new Button("Done");
	
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        doneButton.setOnAction(e->{
            ChangeMapDimsDialog.this.close();
        });
        
        // NOW ORGANIZE OUR STUFF
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(doneButton);
        widthInput = new TextField();
        heightInput = new TextField();
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(widthInput); 
        messagePane.getChildren().add(messageLabel2);
        messagePane.getChildren().add(heightInput);
        messagePane.getChildren().add(buttonBox);

        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return User input
     */
    public String getUserWidth() {
        return widthInput.getText();
    }    
    public String getUserHeight() {
        return heightInput.getText();
    }
    public void clear(){
        widthInput.clear();
        heightInput.clear();
    }
    
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     * @param message2 2nd message
     */
    public void show(String title, String message, String message2) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
	
	// SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);
        messageLabel2.setText(message2);
	
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
}
