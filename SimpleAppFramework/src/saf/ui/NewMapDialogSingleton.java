/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saf.ui;

import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polygon;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Calvin Li
 */
public class NewMapDialogSingleton extends Stage {
    static NewMapDialogSingleton singleton;
        
    // GUI CONTROLS FOR OUR DIALOG
    VBox newMapPane;
    Scene newMapScene;
    Label newMapLabel;
    TextField userInput;
    Button doneButton;
    TextField regionNameTF;
    File selectedJsonFile;
    File selectedDirectory;
    Button okButton;
    boolean doWork = false;
    
    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    private NewMapDialogSingleton() {}
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static NewMapDialogSingleton getSingleton() {
	if (singleton == null)
	    singleton = new NewMapDialogSingleton();
	return singleton;
    }
	
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // Asking for name of the Region.
        VBox topPane = new VBox();
        topPane.setSpacing(5);
        Label regionName = new Label("Region name: ");
        regionNameTF = new TextField();

        // Selecting Parent Directory and File Coordinates.
        Label coordsFileLabel = new Label("Select Coordinates File: ");
        Label parentDir = new Label("Parent region directory: ");
        Button coordsFileBtn = new Button();
        Button parentDirBtn = new Button();
        coordsFileBtn.setGraphic(new ImageView(new Image("file:./images/Open.png")));
        parentDirBtn.setGraphic(new ImageView(new Image("file:./images/Open.png")));
        
        BorderPane midPane = new BorderPane();
        HBox files = new HBox();
        HBox dirBox = new HBox();
        files.getChildren().addAll(coordsFileLabel,coordsFileBtn);
        dirBox.getChildren().addAll(parentDir,parentDirBtn);
        midPane.setTop(files);
        midPane.setBottom(dirBox);
        
        topPane.getChildren().addAll(regionName,regionNameTF);
        
        HBox bottomPane = new HBox();
        okButton = new Button("Ok");
        Button closeButton = new Button("Close");
        bottomPane.getChildren().addAll(okButton, closeButton);
        bottomPane.setSpacing(10);
        
        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        // First; to retrieve any necessary data from the user selections
        coordsFileBtn.setOnAction(e->{
            FileChooser fc = new FileChooser();
            fc.setTitle("Select Coordinates File");
            fc.getExtensionFilters().addAll(
                    //work file ext. descrip., then work file ext. (in this case json)
                    new ExtensionFilter("Map Coordinates (.json)","*.json"));
            selectedJsonFile = fc.showOpenDialog(singleton);
            
        });
        
        parentDirBtn.setOnAction(e->{
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Select Parent Directory");
            selectedDirectory = dc.showDialog(null);
        });
        
        // Then; the "close" and "ok" buttons
        closeButton.setOnAction(e->{
            doWork = false;
            NewMapDialogSingleton.this.close();
        });
        
//        //Ok button done elsewhere.
//        okButton.setOnAction(e->{
//            doWork = true;
//            NewMapDialogSingleton.this.close();
//        });

        // WE'LL PUT EVERYTHING HERE
        newMapPane = new VBox();
        newMapPane.setAlignment(Pos.CENTER);
        newMapPane.getChildren().add(topPane);
        newMapPane.getChildren().add(midPane);
        newMapPane.getChildren().add(bottomPane);

        
        // MAKE IT LOOK NICE
        newMapPane.setPadding(new Insets(20, 20, 20, 20));
        newMapPane.setSpacing(20);
        newMapPane.setStyle("-fx-background-color: #7de5f4; -fx-text-fill: white;");
        // AND PUT IT IN THE WINDOW
        newMapScene = new Scene(newMapPane);
        this.setScene(newMapScene);
    }

    /**
     * Accessor method for getting the text that the user entered.
     * 
     * @return region name entered in the text field.
     */
    public String getSelectedName() {
        return regionNameTF.getText();
    }    
    
    public File getSelectedDirectory() {
        return selectedDirectory;
    }
    
    public File getJsonFile() {
        return selectedJsonFile;
    }
    
    public boolean isDoingWork() {
        return doWork;
    }
    
    public Button getOkButton(){
        return okButton;
    }
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     */
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
}
